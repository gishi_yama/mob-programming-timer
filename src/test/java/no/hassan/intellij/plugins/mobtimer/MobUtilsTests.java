package no.hassan.intellij.plugins.mobtimer;

import static no.hassan.intellij.plugins.mobtimer.MobUtils.toDaysHoursAndMinutes;
import static no.hassan.intellij.plugins.mobtimer.MobUtils.toHoursAndMinutes;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.concurrent.TimeUnit;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

@DisplayName("Testing the MobUtils methods")
class MobUtilsTests {

  private static final int MINUTE = 60; // seconds
  private static final int HOUR = 60 * MINUTE;
  private static final int DAY = 24 * HOUR;

  @Test
  void to1DaysHoursAndMinutesTest() {
    Tipple<Long, Long, Long> daysHoursAndMinutes = toDaysHoursAndMinutes(86_400);
    assertEquals(1L, daysHoursAndMinutes.p1.longValue());
    assertEquals(0L, daysHoursAndMinutes.p2.longValue());
    assertEquals(0L, daysHoursAndMinutes.p3.longValue());
  }

  @Test
  void to3Days5HoursAnd46MinutesTest() {
    Tipple<Long, Long, Long> daysHoursAndMinutes =
        toDaysHoursAndMinutes((3 * DAY) + (5 * HOUR) + (46 * MINUTE));
    assertEquals(3L, daysHoursAndMinutes.p1.longValue());
    assertEquals(5L, daysHoursAndMinutes.p2.longValue());
    assertEquals(46L, daysHoursAndMinutes.p3.longValue());
  }

  @Test
  void to24HoursAndMinutesTest() {
    Pair<Long, Long> hoursAndMinutes = toHoursAndMinutes(24 * HOUR);
    assertEquals(24L, hoursAndMinutes.p1.longValue());
    assertEquals(0L, hoursAndMinutes.p2.longValue());
  }

  @Test
  void to24HoursAnd5MinutesTest() {
    Pair<Long, Long> hoursAndMinutes = toHoursAndMinutes(DAY + 5 * 60);
    assertEquals(24L, hoursAndMinutes.p1.longValue());
    assertEquals(5L, hoursAndMinutes.p2.longValue());
  }

  @Test
  void toMinutesAndSeconds() {
    long millis = TimeUnit.SECONDS.toMillis(60 * 5 + 30);
    Pair<Integer, Byte> minutesAndSeconds = MobUtils.toMinutesAndSeconds(millis);
    assertEquals(5, minutesAndSeconds.p1.intValue());
    assertEquals(30, minutesAndSeconds.p2.intValue());
  }
}
