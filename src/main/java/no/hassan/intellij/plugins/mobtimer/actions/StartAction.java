package no.hassan.intellij.plugins.mobtimer.actions;

import com.intellij.openapi.project.Project;
import com.intellij.openapi.ui.Messages;
import no.hassan.intellij.plugins.mobtimer.Constants;
import no.hassan.intellij.plugins.mobtimer.Mobber;
import no.hassan.intellij.plugins.mobtimer.MobbingManager;
import org.jetbrains.annotations.NotNull;

import javax.swing.JTable;
import java.awt.event.ActionEvent;
import java.util.Optional;

import static no.hassan.intellij.plugins.mobtimer.Constants.UI_Texts.MSG_RESTART;
import static no.hassan.intellij.plugins.mobtimer.Constants.UI_Texts.MSG_RESTART_SWITCH;
import static no.hassan.intellij.plugins.mobtimer.Constants.UI_Texts.MSG_START;
import static no.hassan.intellij.plugins.mobtimer.Constants.UI_Texts.MSG_START_UNDER_5;
import static no.hassan.intellij.plugins.mobtimer.Constants.UI_Texts.TITLE_ERROR;

public class StartAction extends AbstractAction {

  private final JTable mobbersTable;

  public StartAction(
      @NotNull Project project,
      @NotNull MobbingManager mobbingManager,
      @NotNull JTable mobbersTable) {
    super(project, mobbingManager);
    this.mobbersTable = mobbersTable;
  }

  @Override
  public void actionPerformed(ActionEvent e) {
    if (manager.getIntervalLength() < 5) {
      if (manager.getMode().equals(Constants.Mode.PROD)) {
        showInfoMessage(TITLE_ERROR, MSG_START_UNDER_5);
        return;
      }
    }
    int selectedRow = mobbersTable.getSelectedRow();
    if (selectedRow < 0 || selectedRow > mobbersTable.getRowCount()) {
      showInfoMessage(TITLE_ERROR, MSG_START);
      return;
    }
    Mobber mobber = manager.getMobber(selectedRow);
    Optional<Mobber> currentMobber = manager.getCurrentMobber();
    if (currentMobber.isPresent()) {
      Mobber m = currentMobber.get();
      if (m.equals(mobber)) {
        int what = showOkCancelDialog(MSG_RESTART);
        if (what == Messages.OK) {
          manager.restart();
        }
      } else {
        int nowWhat =
            showOkCancelDialog(
                String.format(MSG_RESTART_SWITCH, currentMobber.get().getName(), mobber.getName()));
        if (nowWhat == Messages.OK) {
          manager.forceSwitchToMobber(mobber);
        }
      }
    } else {
      manager.setCurrentMobber(mobber);
    }
  }
}
