package no.hassan.intellij.plugins.mobtimer;

import java.util.Arrays;
import java.util.List;

public interface Constants {

  String MOB_FILE_NAME = ".mob.json";

  List<String> WelcomeMessages =
      Arrays.asList(
          "%s is on the wheel everybody, now watch and learn",
          "%s, time to inspire others",
          "%s, drive safe.",
          "%s, good to have you back on wheels",
          "%s, it's finally you",
          "%s, let's kick some keys");
  List<String> LeavingMessages =
      Arrays.asList(
          "Keep your hands off keyboard %s, it's for %s to take control",
          "%s, your timer is up, take the keyboard %s",
          "Ah ah ah ah, stop %s, it's %s turn",
          "%s, you did really good, now let %s do some good",
          "Stop it, %s stop!!!, it's %s turn",
          "%s you did great, let %s do the same",
          "That went well %s, let's see how %s do");

  interface Mode {
    String PROD = "prod";
    String DEV = "dev";
  }

  interface UI_Texts {
    String TITLE_ERROR = "Now What!!!";
    String TITLE_SWITCH = "Time to Switch!";
    String TITLE_RETROSPECT = "Time to Retrospect!";
    String TITLE_EXPORT = "How Is It Possible!";
    String BTN_GOT_IT = "Got it!";
    String BTN_OK = "That's right";
    String BTN_CANCEL = "Oops, No";
    String BTN_SKIP = "Skip";
    String BTN_PAUSE = "Pause";
    String BTN_RESUME = "Resume";
    String MSG_UNAVAILABLE = "Mobbing conditions are not suitable enough to continue.";
    String MSG_INVALID_NAME = "Write a proper name, wake up!";
    String MSG_TOTAL_SESSION = "Total mobbing time = ";
    String MSG_THIS_SESSION = "This session = ";
    String MSG_CLEAR = "What? You want to delete the complete history?";
    String MSG_PAUSE = "Whom to pause or resume again? TELL ME!!!!";
    String MSG_REMOVE = "Hold on!!!, you are going to remove %s, are you sure?";
    String MSG_START = "Please select a mobber and then press start.";
    String MSG_START_UNDER_5 =
        "Set Time per mobber(min) > 5, Press Update -> select a mobber -> Press Start";
    String MSG_RESTART = "Hey, Do you really want to restart your turn?";
    String MSG_RESTART_SWITCH = "%s, you are not finished yet, do you want to handover to %s";
    String MSG_STOP = "What??? You want to stop now? Are you sure?";
    String MSG_STOP_RETROSPECT =
        "How did it go? Do a retrospect and identify what went well and what didn't.";
    String MSG_UPDATE_TOO_SMALL = "It won't work here, the value must be great than 5";
    String MSG_UPDATE_INVALID =
        "Invalid value for interval, it must be an positive integer, wake up.";
    String MSG_EXPORT_ERROR = "Unable to export history for reason: ";
    String MSG_EXPORT_ERROR_2 = ", Contact the authorities (I mean the maintainer of this plugin)!";
    String MSG_EXPORT_ERROR_NO_IDEA = "No idea, it must had exported but it didn't.";
  }

  interface Durations {
    int ONE_SECOND = 1000;
    int ONE_MINUTE = 60 * ONE_SECOND;
  }

  interface Persistence {
    String KEY_MOBBER_NAME = "mn";
    String KEY_CURRENT_SESSION_TIME = "cst";
  }

  interface MobbingEvents {
    String SHUFFLED = "sfl";
    String CLEAR = "clr";
    String ADDED = "add";
    String REMOVED = "rmv";
    String CHANGED = "chg";
    String SKIPPED = "skp";
    String UPDATED = "upd";
    String PAUSED = "pas";
    String RESUMED = "rsm";
    String RESTARTED = "rst";
    String STOPPED = "stp";
    String UNAVAILABLE = "nop";
    String EXPORTED = "exp";
  }
}
