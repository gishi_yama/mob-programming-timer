package no.hassan.intellij.plugins.mobtimer;

import com.google.gson.JsonObject;
import org.jetbrains.annotations.NotNull;

import java.util.Objects;
import java.util.Optional;
import java.util.StringJoiner;

public class Mobber implements Comparable<Mobber> {

  private String name;
  private int turnCount;
  private int skipCount;
  private boolean left;

  public Mobber(String name) {
    this.name = name;
  }

  static Optional<Mobber> parse(JsonObject mobberJson) {
    if (mobberJson != null) {
      if (mobberJson.has("name")) {
        Mobber mobber = new Mobber(mobberJson.get("name").getAsString());
        if (mobberJson.has("turns")) {
          mobber.setTurnCount(mobberJson.get("turns").getAsInt());
        } else {
          mobber.setTurnCount(0);
        }
        if (mobberJson.has("skips")) {
          mobber.setSkipCount(mobberJson.get("skips").getAsInt());
        } else {
          mobber.setSkipCount(0);
        }
        if (mobberJson.has("left")) {
          mobber.setLeft(mobberJson.get("left").getAsBoolean());
        }
        return Optional.of(mobber);
      }
    }
    return Optional.empty();
  }

  public String getName() {
    return name;
  }

  void setName(String name) {
    this.name = name;
  }

  public int getTurnCount() {
    return turnCount;
  }

  public void setTurnCount(int turnCount) {
    this.turnCount = turnCount;
  }

  public void incrementTurn() {
    turnCount++;
  }

  public int getSkipCount() {
    return skipCount;
  }

  public void setSkipCount(int skipCount) {
    this.skipCount = skipCount;
  }

  public void incrementSkip() {
    skipCount++;
  }

  public boolean isLeft() {
    return left;
  }

  void setLeft(boolean left) {
    this.left = left;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof Mobber)) {
      return false;
    }
    Mobber that = (Mobber) o;
    return getName().equals(that.getName());
  }

  @Override
  public int hashCode() {
    return Objects.hash(getName());
  }

  @Override
  public int compareTo(@NotNull Mobber that) {
    return that.getName().compareTo(name);
  }

  @Override
  public String toString() {
    return new StringJoiner(", ")
        .add("name='" + name + "'")
        .add("turnCount=" + turnCount)
        .add("skipCount=" + skipCount)
        .add("left=" + left)
        .toString();
  }

  JsonObject toJson() {
    JsonObject json = new JsonObject();
    json.addProperty("name", name);
    json.addProperty("turns", turnCount);
    json.addProperty("skips", skipCount);
    json.addProperty("left", left);
    return json;
  }

  Object[] toDataRow() {
    return new Object[] {name, turnCount, skipCount, left};
  }
}
