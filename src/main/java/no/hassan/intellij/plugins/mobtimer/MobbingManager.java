package no.hassan.intellij.plugins.mobtimer;

import com.intellij.openapi.application.ApplicationListener;
import com.intellij.openapi.diagnostic.Logger;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.project.ProjectManagerListener;
import no.hassan.intellij.plugins.mobtimer.history.Exporter;
import no.hassan.intellij.plugins.mobtimer.history.Importer;
import no.hassan.intellij.plugins.mobtimer.history.JsonExporter;
import no.hassan.intellij.plugins.mobtimer.history.JsonImporter;
import org.jetbrains.annotations.NotNull;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import static com.intellij.openapi.ui.Messages.showWarningDialog;
import static java.lang.Boolean.FALSE;
import static java.lang.Boolean.TRUE;
import static no.hassan.intellij.plugins.mobtimer.Constants.Durations.ONE_SECOND;
import static no.hassan.intellij.plugins.mobtimer.Constants.MOB_FILE_NAME;
import static no.hassan.intellij.plugins.mobtimer.Constants.MobbingEvents.ADDED;
import static no.hassan.intellij.plugins.mobtimer.Constants.MobbingEvents.CHANGED;
import static no.hassan.intellij.plugins.mobtimer.Constants.MobbingEvents.CLEAR;
import static no.hassan.intellij.plugins.mobtimer.Constants.MobbingEvents.PAUSED;
import static no.hassan.intellij.plugins.mobtimer.Constants.MobbingEvents.REMOVED;
import static no.hassan.intellij.plugins.mobtimer.Constants.MobbingEvents.RESTARTED;
import static no.hassan.intellij.plugins.mobtimer.Constants.MobbingEvents.RESUMED;
import static no.hassan.intellij.plugins.mobtimer.Constants.MobbingEvents.SHUFFLED;
import static no.hassan.intellij.plugins.mobtimer.Constants.MobbingEvents.SKIPPED;
import static no.hassan.intellij.plugins.mobtimer.Constants.MobbingEvents.STOPPED;
import static no.hassan.intellij.plugins.mobtimer.Constants.MobbingEvents.UNAVAILABLE;
import static no.hassan.intellij.plugins.mobtimer.Constants.MobbingEvents.UPDATED;
import static no.hassan.intellij.plugins.mobtimer.Constants.UI_Texts.MSG_EXPORT_ERROR;
import static no.hassan.intellij.plugins.mobtimer.Constants.UI_Texts.MSG_EXPORT_ERROR_2;
import static no.hassan.intellij.plugins.mobtimer.Constants.UI_Texts.MSG_EXPORT_ERROR_NO_IDEA;
import static no.hassan.intellij.plugins.mobtimer.Constants.UI_Texts.TITLE_EXPORT;

@SuppressWarnings("OptionalUsedAsFieldOrParameterType")
public class MobbingManager implements ProjectManagerListener, ApplicationListener {

  private static final Logger logger = Logger.getInstance(MobbingManager.class);

  private final Project project;
  private final Exporter exporter = new JsonExporter();
  private List<Mobber> mobbers;
  private Optional<Mobber> currentMobber = Optional.empty();
  private int currentMobberIndex;
  private int intervalLength;
  private long totalMobbingInSeconds;
  private long currentSessionTime;
  private boolean running;
  private PropertyChangeSupport changeSupport = new PropertyChangeSupport(this);
  private String mode;

  public MobbingManager(@NotNull Project project, String mode) {
    this.mode = mode;
    this.project = project;
    currentMobberIndex = -1;
    mobbers = new ArrayList<>();
    Importer importer = new JsonImporter();
    Model model =
        importer.load(Paths.get(Objects.requireNonNull(project.getBasePath()), MOB_FILE_NAME));
    mobbers = model.getMobbers();
    intervalLength = model.getInterval();
    totalMobbingInSeconds = model.getTotalMobbingInSeconds();
  }

  int getCurrentMobberIndex() {
    return currentMobberIndex;
  }

  public void stopMobbing() {
    setRunning(false);
    currentMobber = Optional.empty();
    currentMobberIndex = -1;
    currentSessionTime = 0;
    fireEvent(STOPPED, TRUE, FALSE);
  }

  private void fireEvent(String eventName, Object oldValue, Object newValue) {
    changeSupport.firePropertyChange(eventName, oldValue, newValue);
  }

  public void addToTotalMobTiming(long millis) {
    currentSessionTime += (millis / ONE_SECOND);
    totalMobbingInSeconds += (millis / ONE_SECOND);
    export();
  }

  public void addMobber(String name) {
    if (name == null || name.trim().length() == 0 || mobberExists(name)) {
      return;
    }
    Mobber mobber = new Mobber(name);
    mobbers.add(mobber);
    export();
    fireEvent(ADDED, null, mobber);
  }

  public void removeMobber(int rowIndex) {
    if (rowIndex < 0 || rowIndex > mobbers.size() - 1) {
      return;
    }
    mobbers.remove(rowIndex);
    fireEvent(REMOVED, rowIndex, -1);
    export();
  }

  public void shuffle() {
    List<Mobber> olderValue = getOlderMobbers();
    Collections.shuffle(mobbers);
    currentMobber.ifPresent(mobber -> currentMobberIndex = mobbers.indexOf(mobber));
    fireEvent(SHUFFLED, olderValue, mobbers);
    export();
  }

  public int getIntervalLength() {
    return intervalLength;
  }

  public void setIntervalLength(int intervalLength) {
    int olderValue = this.intervalLength;
    this.intervalLength = intervalLength;
    fireEvent(UPDATED, olderValue, this.intervalLength);
    export();
  }

  public void export() {
    boolean notExported;
    try {
      FileOutputStream stream =
          new FileOutputStream(new File(project.getBasePath(), MOB_FILE_NAME));
      notExported = !exporter.export(stream, getModel());
    } catch (IOException ex) {
      String message = MSG_EXPORT_ERROR + ex.getMessage();
      logger.error(message, ex);
      showWarningDialog(project, message + MSG_EXPORT_ERROR_2, TITLE_EXPORT);
      notExported = true;
    }
    if (notExported) {
      showWarningDialog(project, MSG_EXPORT_ERROR_NO_IDEA, TITLE_EXPORT);
    }
  }

  public Model getModel() {
    return new Model(intervalLength, totalMobbingInSeconds, mobbers);
  }

  public List<Mobber> getMobbers() {
    return mobbers;
  }

  private boolean mobberExists(String name) {
    return mobbers.stream().anyMatch(m -> m.getName().equals(name));
  }

  @NotNull
  private List<Mobber> getOlderMobbers() {
    return new ArrayList<>(mobbers);
  }

  public Mobber getMobber(int selectedRow) {
    return mobbers.get(selectedRow);
  }

  public Optional<Mobber> getCurrentMobber() {
    return currentMobber;
  }

  public void setCurrentMobber(Mobber mobber) {
    // Must have enough mobbers
    boolean req1 = !mobbers.isEmpty() && mobbers.size() > 2;
    // Must have enough that have not left
    boolean req2 = mobbers.stream().filter(m -> !m.isLeft()).count() > 2;
    // Give mobber is valid enough
    boolean req3 = mobber != null && !mobber.isLeft() && mobbers.contains(mobber);
    if (!req1 || !req2 || !req3) {
      fireEvent(UNAVAILABLE, null, null);
    } else {
      currentMobber = Optional.of(mobber);
      currentMobberIndex = mobbers.indexOf(mobber);
      updateCurrentMobber();
      fireEvent(CHANGED, null, currentMobber.orElse(null));
    }
  }

  private void updateCurrentMobber() {
    currentMobber.ifPresent(m -> m.setTurnCount(m.getTurnCount() + 1));
    running = true;
    export();
  }

  public void addPropertyChangeListener(PropertyChangeListener listener) {
    changeSupport.addPropertyChangeListener(listener);
  }

  private void selectNext(int originalIndex, int currentIndex) {
    if (mobbers.stream().filter(mobber -> !mobber.isLeft()).count() <= 1) {
      fireEvent(UNAVAILABLE, null, null);
      return;
    }
    int nextIndex = 0;
    if (currentIndex < (mobbers.size() - 1)) {
      nextIndex = currentIndex + 1;
    }
    Mobber nextMobber = mobbers.get(nextIndex);
    if (nextMobber.isLeft()) {
      selectNext(originalIndex, nextIndex);
    } else {
      currentMobber = Optional.ofNullable(mobbers.get(nextIndex));
      currentMobber.ifPresent(mobber -> currentMobberIndex = mobbers.indexOf(mobber));
      updateCurrentMobber();
      fireEvent(CHANGED, mobbers.get(originalIndex), currentMobber.orElse(null));
    }
  }

  public void restart() {
    currentMobber.ifPresent(
        mobber -> {
          mobber.setSkipCount(mobber.getSkipCount() + 1);
          mobber.setTurnCount(mobber.getTurnCount() + 1);
          updateCurrentMobber();
          fireEvent(RESTARTED, mobber, currentMobber.orElse(null));
        });
  }

  public boolean isRunning() {
    return running;
  }

  public void setRunning(boolean running) {
    this.running = running;
    if (running) {
      fireEvent(RESUMED, TRUE, FALSE);
    } else {
      fireEvent(PAUSED, TRUE, FALSE);
    }
  }

  int indexOf(Mobber mobber) {
    return mobbers.indexOf(mobber);
  }

  void nameUpdated(int index, String name) {
    Mobber mobber = mobbers.get(index);
    if (mobber != null && !mobber.getName().equals(name)) {
      mobber.setName(name);
      export();
    }
  }

  void statusUpdated(int index, Boolean gone) {
    Mobber mobber = mobbers.get(index);
    if (mobber != null && mobber.isLeft() != gone) {
      mobber.setLeft(gone);
      export();
    }
  }

  public void countdownFinished() {
    selectNext(currentMobberIndex, currentMobberIndex);
  }

  public long getTotalMobTiming() {
    return totalMobbingInSeconds;
  }

  public void forceSwitchToMobber(Mobber mobber) {
    if (mobbers.contains(mobber)) {
      currentMobber.ifPresent(m -> m.setSkipCount(m.getSkipCount() + 1));
      fireEvent(SKIPPED, currentMobber.orElse(null), null);
      setCurrentMobber(mobber);
    }
  }

  public void skipCurrentMobber(Mobber mobber) {
    mobber.setSkipCount(mobber.getSkipCount() + 1);
    mobber.setTurnCount(mobber.getTurnCount() - 1);
    mobbers.set(mobbers.indexOf(mobber), mobber);
    fireEvent(SKIPPED, mobber, null);
    selectNext(mobbers.indexOf(mobber), mobbers.indexOf(mobber));
    export();
  }

  public void flashAll() {
    mobbers.clear();
    intervalLength = 10;
    currentMobber = Optional.empty();
    currentMobberIndex = -1;
    setRunning(false);
    totalMobbingInSeconds = 0;
    fireEvent(CLEAR, null, null);
    export();
  }

  @Override
  public void projectClosing(@NotNull Project project) {
    if (project.equals(this.project)) {
      stopMobbing();
    }
  }

  @Override
  public void applicationExiting() {
    stopMobbing();
  }

  public String getMode() {
    return mode;
  }
}
