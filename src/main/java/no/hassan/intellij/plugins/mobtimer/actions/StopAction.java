package no.hassan.intellij.plugins.mobtimer.actions;

import com.intellij.openapi.project.Project;
import com.intellij.openapi.ui.Messages;
import no.hassan.intellij.plugins.mobtimer.MobbingManager;
import org.jetbrains.annotations.NotNull;

import java.awt.event.ActionEvent;

import static no.hassan.intellij.plugins.mobtimer.Constants.UI_Texts.MSG_STOP;
import static no.hassan.intellij.plugins.mobtimer.Constants.UI_Texts.MSG_STOP_RETROSPECT;
import static no.hassan.intellij.plugins.mobtimer.Constants.UI_Texts.TITLE_RETROSPECT;

public class StopAction extends AbstractAction {

  public StopAction(@NotNull Project project, @NotNull MobbingManager mobbingManager) {
    super(project, mobbingManager);
  }

  @Override
  public void actionPerformed(ActionEvent e) {
    if (showOkCancelDialog(MSG_STOP) == Messages.OK) {
      manager.stopMobbing();
      showInfoMessage(TITLE_RETROSPECT, MSG_STOP_RETROSPECT);
    }
  }
}
