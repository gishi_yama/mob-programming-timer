package no.hassan.intellij.plugins.mobtimer;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Model {

  private int interval;
  private long totalMobbingInSeconds;
  private List<Mobber> mobbers;

  public Model(int interval, long totalMobbingInSeconds, List<Mobber> mobbers) {
    this.interval = interval;
    this.totalMobbingInSeconds = totalMobbingInSeconds;
    if (mobbers != null) {
      this.mobbers = mobbers;
    } else {
      this.mobbers = new ArrayList<>();
    }
  }

  public Model(JsonObject modelJson) {
    if (modelJson.has("interval")) {
      interval = modelJson.get("interval").getAsInt();
    }
    if (modelJson.has("totalMobbingInSeconds")) {
      totalMobbingInSeconds = modelJson.get("totalMobbingInSeconds").getAsLong();
    }
    if (modelJson.has("mobbers")) {
      JsonArray mobbers = modelJson.getAsJsonArray("mobbers");
      if (mobbers != null) {
        this.mobbers = new ArrayList<>(mobbers.size());
        for (int i = 0; i < mobbers.size(); i++) {
          JsonObject mobberJson = (JsonObject) mobbers.get(i);
          Mobber.parse(mobberJson).ifPresent(this.mobbers::add);
        }
      }
    }
  }

  public long getTotalMobbingInSeconds() {
    return totalMobbingInSeconds;
  }

  public int getInterval() {
    return interval;
  }

  public List<Mobber> getMobbers() {
    return mobbers;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof Model)) {
      return false;
    }
    Model model = (Model) o;
    return interval == model.interval
        && totalMobbingInSeconds == model.totalMobbingInSeconds
        && mobbers.equals(model.mobbers);
  }

  @Override
  public int hashCode() {
    return Objects.hash(interval, totalMobbingInSeconds, mobbers);
  }
}
