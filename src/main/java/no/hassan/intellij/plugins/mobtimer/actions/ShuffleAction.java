package no.hassan.intellij.plugins.mobtimer.actions;

import com.intellij.openapi.project.Project;
import no.hassan.intellij.plugins.mobtimer.MobbingManager;
import org.jetbrains.annotations.NotNull;

import java.awt.event.ActionEvent;

public class ShuffleAction extends AbstractAction {

  public ShuffleAction(@NotNull Project project, @NotNull MobbingManager mobbingManager) {
    super(project, mobbingManager);
  }

  @Override
  public void actionPerformed(ActionEvent e) {
    if (!manager.getMobbers().isEmpty()) {
      manager.shuffle();
    }
  }
}
