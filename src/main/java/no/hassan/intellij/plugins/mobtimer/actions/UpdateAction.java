package no.hassan.intellij.plugins.mobtimer.actions;

import com.intellij.openapi.project.Project;
import no.hassan.intellij.plugins.mobtimer.Constants;
import no.hassan.intellij.plugins.mobtimer.MobbingManager;
import org.jetbrains.annotations.NotNull;

import javax.swing.JTextField;
import java.awt.event.ActionEvent;

import static no.hassan.intellij.plugins.mobtimer.Constants.UI_Texts.MSG_UPDATE_INVALID;
import static no.hassan.intellij.plugins.mobtimer.Constants.UI_Texts.MSG_UPDATE_TOO_SMALL;
import static no.hassan.intellij.plugins.mobtimer.Constants.UI_Texts.TITLE_ERROR;

public class UpdateAction extends AbstractAction {

  private final JTextField txtIntervalLength;

  public UpdateAction(
          @NotNull Project project,
          @NotNull MobbingManager mobbingManager,
          JTextField txtIntervalLength) {
    super(project, mobbingManager);
    this.txtIntervalLength = txtIntervalLength;
  }

  @Override
  public void actionPerformed(ActionEvent e) {
    String intervalLengthText = txtIntervalLength.getText();
    try {
      int newIntervalInMinutes = Integer.parseInt(intervalLengthText);
      if (newIntervalInMinutes < 6 && manager.getMode().equals(Constants.Mode.PROD)) {
        showInfoMessage(TITLE_ERROR, MSG_UPDATE_TOO_SMALL);
      } else {
        manager.setIntervalLength(newIntervalInMinutes);
      }
    } catch (NumberFormatException nfe) {
      showInfoMessage(TITLE_ERROR, MSG_UPDATE_INVALID);
    }
  }
}
