# Mob Programming Timer
### MOTIVATION
---
This is an effort to encourage and enforce mob programming in a project. 
If you don't know about mob programming, go check out [Mob Programming](https://www.agilealliance.org/glossary/mob-programming).
We had the pleasure to invite **[Woody Zuill]([https://www.agilealliance.org/author/5052028)** to our office at Oslo, Norway. 
We were very much inspired by it and started using it in our own project, 
but we were lacking a tool which we could use on daily basis, 
so I developed this plugin with the hope that it might also help us and other developers.
###  Features
---
- Tabular view in Tool window
- Add/Remove Mobber
- Set time interval for each turn
- Shuffle the mobber list
- Set a mobber to Gone, if she has another meeting
- Keep track of active mobbing session
- Keep track of total mobbing time per project
- Pause or Resume timer anytime
- Switch to another mobber anytime by selecting her from the table and pressing start
- Ability to skip a mobber
### Screenshots
![Screenshot 1](./screenshots/s1.png)
![Screenshot 2](./screenshots/s2.png)
